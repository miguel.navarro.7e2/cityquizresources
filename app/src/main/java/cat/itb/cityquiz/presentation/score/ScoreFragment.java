package cat.itb.cityquiz.presentation.score;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.question.CityQuizViewModel;

public class ScoreFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    Game gameFinish;
    TextView score;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this, this::display);

    }

    private void display(Game game) {
        score.setText(String.valueOf(game.getNumCorrectAnswers()));
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        score = getView().findViewById(R.id.scores);
    }

    @OnClick(R.id.goMenu)
    public void returnToMenu(View view) {
        mViewModel.startQuiz();
        Navigation.findNavController(view).navigate(R.id.returnMenu);
    }
}
