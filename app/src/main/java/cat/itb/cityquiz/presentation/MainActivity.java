package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.question.QuestionFragment2;

import static androidx.navigation.Navigation.findNavController;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

    }

}
