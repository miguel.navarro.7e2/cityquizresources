package cat.itb.cityquiz.presentation.question;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {
    MutableLiveData<Game> game = new MutableLiveData<>();
    GameLogic gameLogic;

    public void startQuiz() {
        gameLogic = RepositoriesFactory.getGameLogic();
        Game creGame = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        game.postValue(creGame);
    }

    public MutableLiveData<Game> getGame() {
        return game;
    }

    public void answer(int answer) {
        Game localGame = gameLogic.answerQuestions(game.getValue(), answer);
        game.postValue(localGame);
    }
}
