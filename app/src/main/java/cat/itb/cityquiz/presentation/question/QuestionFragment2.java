package cat.itb.cityquiz.presentation.question;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.GameLogic;


public class QuestionFragment2 extends Fragment {

    private CityQuizViewModel mViewModel;
    private ImageView imageView;
    @BindView(R.id.firstButton)
    MaterialButton materialButton1;
    @BindView(R.id.secondButton)
    MaterialButton materialButton2;
    @BindView(R.id.thirdButton)
    MaterialButton materialButton3;
    @BindView(R.id.fourthButton)
    MaterialButton materialButton4;
    @BindView(R.id.fifthButton)
    MaterialButton materialButton5;
    @BindView(R.id.sixthButton)
    MaterialButton materialButton6;

    public static QuestionFragment2 newInstance() {
        return new QuestionFragment2();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment2, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this, this::display);

    }

    private void display(Game game) {
        if (!mViewModel.getGame().getValue().isFinished()) {
            Question question = game.getCurrentQuestion();
            question.getPossibleCities();
            String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            imageView.setImageResource(resId);

            materialButton1.setText(question.getPossibleCities().get(0).getName());
            materialButton2.setText(question.getPossibleCities().get(1).getName());
            materialButton3.setText(question.getPossibleCities().get(2).getName());
            materialButton4.setText(question.getPossibleCities().get(3).getName());
            materialButton5.setText(question.getPossibleCities().get(4).getName());
            materialButton6.setText(question.getPossibleCities().get(5).getName());
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        imageView = getView().findViewById(R.id.imageBack);
        materialButton1.setOnClickListener(this::answerClicked);
        materialButton2.setOnClickListener(this::answerClicked);
        materialButton3.setOnClickListener(this::answerClicked);
        materialButton4.setOnClickListener(this::answerClicked);
        materialButton5.setOnClickListener(this::answerClicked);
        materialButton6.setOnClickListener(this::answerClicked);


    }

    private void answerClicked(View view) {
        if (view == materialButton1) {
            quiestionAnswered(0);
        } else if (view == materialButton2) {
            quiestionAnswered(1);
        } else if (view == materialButton3) {
            quiestionAnswered(2);
        } else if (view == materialButton4) {
            quiestionAnswered(3);
        } else if (view == materialButton5) {
            quiestionAnswered(4);
        } else {
            quiestionAnswered(5);
        }
        display(mViewModel.getGame().getValue());

        if (mViewModel.getGame().getValue().isFinished()) {
            Navigation.findNavController(view).navigate(R.id.goToScore);
        }


    }

    private void quiestionAnswered(int i) {
        mViewModel.answer(i);
    }
}
