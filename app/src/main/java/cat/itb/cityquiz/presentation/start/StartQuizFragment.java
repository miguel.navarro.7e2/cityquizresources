package cat.itb.cityquiz.presentation.start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.question.CityQuizViewModel;

public class StartQuizFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    private boolean gameReady = false;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);

    }

    private void onGameChanged(Game game) {
        if (game != null) {
            Navigation.findNavController(getView()).navigate(R.id.startQuiz);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.buttonStart)
    public void changeFragment(View view) {
        mViewModel.startQuiz();
    }

}
