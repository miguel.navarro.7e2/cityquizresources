package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void navigateTest(){
        onView(withId(R.id.buttonStart)).perform(click());

        onView(withId(R.id.imageBack)).check(matches(isDisplayed()));

        onView(withId(R.id.firstButton)).perform(click());
        onView(withId(R.id.secondButton)).perform(click());
        onView(withId(R.id.thirdButton)).perform(click());
        onView(withId(R.id.fourthButton)).perform(click());
        onView(withId(R.id.fifthButton)).perform(click());

        onView(withId(R.id.goMenu)).perform(click());
    }
}
